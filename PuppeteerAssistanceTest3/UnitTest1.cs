namespace PuppeteerAssistance2;

public class Tests
{
    private const string TestFileName = "index.html";
        private PuppeteerAssistance.PuppeteerPlus assistance;
        #region Init and Destruct

        [OneTimeSetUp]
        public async Task OneTimeSetUp()
        {
            var files = Directory.GetFiles(Directory.GetCurrentDirectory());
            var file = files.FirstOrDefault(x => x.Contains(TestFileName));
            
            if (string.IsNullOrEmpty(file))
            {
                throw new FileNotFoundException("The file for testing {index.html} doesn't exist.");
            }

            file = $"file://{file}";
            assistance = await PuppeteerAssistance.PuppeteerPlus.InitAsync();
            await assistance.GoToAsync(file);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            assistance.Dispose();;
        }

        #endregion

        #region UI Interactions

        [Test]
        public async Task Test_ClickAsync2()
        {
            await assistance.ClickAsync("#lovely");
            const string script = "document.querySelector('#take_me').innerText*1";
            const int expected = 1;
            var actual = await assistance._Page.EvaluateExpressionAsync<int>(script);
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public async Task Test_UploadAsync()
        {
            await assistance.UploadAsync("#Upload", TestFileName);
            var script = "document.querySelector('#Upload').value";
            var name_of_file = await assistance._Page.EvaluateExpressionAsync<string>(script);
            var match_found = name_of_file.Contains("index");
            Assert.IsTrue(match_found);
        }
        
        [Test]
        public async Task Test_SelectAsync()
        {
            await assistance.SelectOptionsAsync("#cars", "opel");
            var result = await assistance._Page.EvaluateExpressionAsync<string>("document.querySelector('#cars').value");
            Assert.AreEqual(result, "opel");
        }
        
        [Test]
        public async Task Test_SetCheckboxAsync()
        {
            await assistance.SetCheckboxAsync("#checkbox", false);
        }
        
        [Test]
        public async Task Test_TypeAsync()
        {
            await assistance.TypeAsync("#text", "Love");
            var value = await assistance._Page.EvaluateExpressionAsync<string>("document.querySelector('#text').value");
            Assert.AreEqual("Love", value);
        }
        
        #endregion

        #region Collections
        [Test]
        
        public async Task Test_GetStringListFromScript()
        {
            string script = "[...document.querySelector('#list2').children].map(x => x.innerText)";
            List<string> list = await assistance.GetStringListFromScript(script);
            Assert.AreEqual(8, list.Count);
        }
        
        [Test]
        public async Task Test_GetHashsetFromScript()
        {
            string script = "[...document.querySelector('#list2').children].map(x => x.innerText)";
            var hashset = await assistance.GetHashsetFromScript(script, 604);
            Assert.AreEqual(7, hashset.Count);
        }
        
        [Test]
        public async Task Test_GetDictionaryFromScript()
        {
            string script = "var dict = {}; [...document.querySelectorAll('#love123 > p')].map(x => x.innerText).filter(x => x != '').map((x,i,l) => dict[x] = i); dict;";
            var dictionary = await assistance.GetDictionaryFromScript(script, 604);
            Assert.AreEqual(200, dictionary.Count);
        }
        
        [Test]
        public async Task Test_GetDictionaryFromList()
        {
            string script = "[...document.querySelectorAll('#love123 > p')].map(x => x.innerText).filter(x => x != '').map((x,i,l) => x +',' + i);";
            var dictionary = await assistance.GetDictionaryFromList(script, 604);
            Assert.AreEqual(200, dictionary.Count);
        }
        
        #endregion
        
        /*
         
        [Test]
        public async Task IsPopUpWork()
        {
            //
            Func<Page, Task> func = async (Page p) =>
            {
                await assistance.p.GoToAsync(Url);
                await PuppeteerAssistance.PuppeteerPlus.ClickAsync(p, "#lovely2");
                using var page = await PuppeteerAssistance.PuppeteerPlus.GetPopUpPage(p);
                var value = ((string)(await page.EvaluateExpressionAsync("window.location.href")));
                await page.CloseAsync();
                Assert.IsTrue(value.ToLower().Contains("google"));
            };
            await PuppeteerAssistance.PuppeteerPlus.Perform(func);
            await PuppeteerAssistance.PuppeteerPlus.Close();
        }
        
        */
}