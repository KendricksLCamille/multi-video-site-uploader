﻿#define LINUX

using System.Diagnostics;
using PuppeteerExtraSharp;
using PuppeteerExtraSharp.Plugins.ExtraStealth;
using PuppeteerSharp;
using PuppeteerSharp.Input;

namespace PuppeteerAssistance;

public class PuppeteerPlus : IDisposable
{
    private Page _page = null!;
    private Browser _browser = null!;

    public Page _Page => _page;
    
    #region Initialization and Destruction Methods

    public static async Task<PuppeteerPlus> InitAsync(bool isHeadless = false)
    {
        // Get a browser
        await new BrowserFetcher().DownloadAsync();
        
        // Initialization plugin builder
        var extra = new PuppeteerExtra();

        // Use stealth plugin
        extra.Use(new StealthPlugin());

#if TARGET_LINUX && !TARGET_ANDROID
        string[] arguments = new[] { "--no-sandbox" };
#else
        //AlternativeS to check at runtime: https://stackoverflow.com/questions/38790802/determine-operating-system-in-net-core 
        string[] arguments = new String[] {};
#endif

        var pp = new PuppeteerPlus();
        
        // Launch the puppeteer browser with plugins
        pp._browser = (Browser)await extra.LaunchAsync(new LaunchOptions() {
            Headless = isHeadless,
            Args = arguments
        });

        //Use First Page
        pp._page = (Page)(await pp._browser.PagesAsync())[0];

        return pp;
    }

    public void Dispose()
    {
        _page.Dispose();
        _browser.Dispose();
    }

    #endregion

    #region Miscelleneous

    public Page Page => _page;

    static int SecondsToMilliseconds(int milliseconds) => milliseconds * 1000;

    private async Task WaitForSelectorThenIdlePlusTimeAsync(string selector = "body", int waitTimeInMs = 1)
    {
        await _page.WaitForSelectorAsync(selector);

        await Task.WhenAny(new List<Task>() { _page.WaitForNetworkIdleAsync(), _page.WaitForTimeoutAsync( SecondsToMilliseconds(28)) });
        
        //Should be called after everything is done
        await _page.WaitForTimeoutAsync(waitTimeInMs);
    }
    
    private static string GetQuerySelectorScript(string selector) => $"document.querySelector('{selector}')";
    #endregion

    #region Browser
    public async Task GoToAsync(string url, int waitTimeInMs = 1)
    {
        await _page.GoToAsync(url, WaitUntilNavigation.Networkidle2);
        await WaitForSelectorThenIdlePlusTimeAsync(waitTimeInMs: waitTimeInMs);
    }
    #endregion

    #region Page

     /// <summary>
    /// Contains all information needed to perform a click and test if it works.
    /// </summary>
    /// <remarks>All strings set to this type are implicitly converted to selectors.</remarks>
    /// <example>
    /// <code>
    /// string s = "Hi";
    /// ClickInformation ci1 = s;
    /// ClickInformation ci2 = new ClickInformation(){ Selector = s };
    /// ci1.Selector.Equals(ci2.Selector); //true
    /// </code>
    /// </example>
    public record ClickInformation
    {
        /// <summary>
        /// A css selector for what is being clicked.
        /// MUST NOT BE NULL
        /// </summary>
        /// <remarks> Must Not Be Null</remarks>
        public string Selector { get; private init; } = string.Empty;
        
        /// <summary>
        /// Time to wait in milliseconds after the network is idle
        /// </summary>
        public int WaitTimeAfterNetworkIdleInMs { get; set; } = 1;
        
        /// <summary>
        /// Time to wait in milliseconds after each attempt at clicking something
        /// </summary>
        public int WaitTimeBetweenEachTryInMs { get; set; } = 1;

        /// <summary>
        /// Wait time in MS for everything else that can use MS.
        /// It is automatically set to some value between 0 to 10 seconds;
        /// </summary>
        public int WaitTimeGenericInMs { get; set; } = 0;
        
        /// <summary>
        /// Used to set up any data that can be used to test if the click is working if needed
        /// </summary>
        public Func<Task>? SetUpForCheck { get; set; } = default;

        /// <Summary>
        /// Test each attempt to make sure that it actually worked
        /// </Summary>
        public Func<Task<bool>>? Checker { get; set; } = default;
        
        public static implicit operator ClickInformation(string s) => new(){Selector = s};
    }
    
    public async Task ClickAsync(ClickInformation clickInformation)
    {
        Debug.WriteLine("Starting Click Method");
        string selector = clickInformation.Selector ?? throw new NullReferenceException("Selector is Null");
        await WaitForSelectorThenIdlePlusTimeAsync(selector,clickInformation.WaitTimeAfterNetworkIdleInMs);

        Debug.WriteLine($"Done waiting for selector:({selector}) to show up.");

        #region Set Up Check Functions
        const string variableName = "check"; //Using a variable in case I run into name collision issue.
        var setUpScript = $"var {variableName} = false; {GetQuerySelectorScript(selector)}.addEventListener('click', (event) => {{{variableName} = true;}})";
        var checkScript = $"{variableName} === true";
        
        // ReSharper disable SuggestVarOrType_Elsewhere
        Func<Task> setUpCheck = clickInformation.SetUpForCheck ?? (async () => await _page.EvaluateExpressionAsync(setUpScript));
        Func<Task<bool>> check = clickInformation.Checker ?? (async () => await _page.EvaluateExpressionAsync<bool>(checkScript));
        // ReSharper restore SuggestVarOrType_Elsewhere
        #endregion

        #region Click_Functions

        async Task ClickWithScript(string selector2)
        {
            var script = $"{GetQuerySelectorScript(selector2)}.click()";
            await _page.EvaluateExpressionAsync(script);
        }
        
        async Task ClickWithMouse(string selector2)
        {
            var elementHandle = await _page.QuerySelectorAsync(selector2);
            var box = await elementHandle.BoundingBoxAsync();
            var x = box.X + box.X / 2;
            var y = box.Y + box.Y / 2;
            await _page.Mouse.ClickAsync(x, y, new ClickOptions() { Delay = clickInformation.WaitTimeGenericInMs });
        }
        
        async Task ClickWithMouse2(string selector2)
        {
            var elementHandle = await _page.QuerySelectorAsync(selector2);
            var box = await elementHandle.BoundingBoxAsync();
            var x = box.X + box.X / 2;
            var y = box.Y + box.Y / 2;
            await _page.Mouse.MoveAsync(x, y);
            await _page.Mouse.DownAsync();
            await _page.WaitForTimeoutAsync(clickInformation.WaitTimeGenericInMs);
            await _page.Mouse.UpAsync();
        }
        
        async Task ClickWithMouse3(string selector2) => await _page.ClickAsync(selector2, new ClickOptions() { Delay = clickInformation.WaitTimeGenericInMs });
        
        async Task ClickWithKeyboard(string selector2)
        {
            await _page.FocusAsync(selector2);
            await _page.Keyboard.TypeAsync("\n");
        }

        #endregion

        List<Func<string, Task>> functions = new()
            { ClickWithKeyboard, ClickWithMouse, ClickWithMouse2, ClickWithMouse3, ClickWithScript };

        await setUpCheck();
        foreach (var func in functions)
        {
            try
            {
                Debug.WriteLine("Attempting to click button with method: " + func.Method.Name);
                await func(selector);
                if (!await check()) continue;
                Debug.WriteLine("End of Click Method");
                return;
            }
            catch { /* ignored */ }
        }

        throw new Exception($"Failed to click the button with the selector:({selector})");
    }

    /// <summary>
    /// Attempts to click a button, trigger the opening of the file dialog and then passing in the path to the file and wait for the upload
    /// </summary>
    /// <param name="selector">The selector of the button being pressed</param>
    /// <param name="pathToFile">The path to the file being uploaded</param>
    /// <param name="checkResult">A function to confirm that the upload was successful else it just immediately returns</param>
    /// <param name="attempts">The number of times to attempt to upload</param>
    /// <param name="waitTime">The length of time in milliseconds to wait between attempts</param>
    public async Task UploadAsync(string selector,
        string pathToFile,
        Func<Task<bool>>? checkResult = null,
        int attempts = 0,
        int waitTime = 10000)
    {
        await WaitForSelectorThenIdlePlusTimeAsync(selector);
        var fileChooserDialogTask = _page.WaitForFileChooserAsync(); // Do not await here
        await Task.WhenAll(fileChooserDialogTask, ClickAsync(selector));

        var fileChooser = await fileChooserDialogTask;
        Console.WriteLine(pathToFile);
        await fileChooser.AcceptAsync(pathToFile);

        if (attempts <= 0)
        {
            await WaitForSelectorThenIdlePlusTimeAsync();
        }
        else
        {
            //wait till value returns true else keep trying till 100 tries
            foreach (var unused in Enumerable.Range(0, attempts))
            {
                var result = checkResult == null || (await checkResult());
                if (result)
                {
                    break;
                }

                await Task.Delay(waitTime);
            }
        }
    }
    
    
    /// <summary>
    /// Select an option in a selector
    /// </summary>
    /// <param name="selector">The css selector for the element</param>
    /// <param name="value">The value of the option being selected</param>
    /// <param name="waitTimeForSelectorInMs">How long to wait before attempting to select an option</param>
    public async Task SelectOptionsAsync(string selector, string value, int waitTimeForSelectorInMs = 0)
    {
        await WaitForSelectorThenIdlePlusTimeAsync(selector, waitTimeForSelectorInMs);
        await _page.SelectAsync(selector, value);
    }
        
    
    public async Task SetCheckboxAsync(string selector, bool valueWanted, int waitTimeForSelectorInMs = 0)
    {
        await WaitForSelectorThenIdlePlusTimeAsync(selector, waitTimeForSelectorInMs);
        var getCheckboxValueScript = $"document.querySelector('{selector}').checked";
        var checkboxValue = ((bool)(await _page.EvaluateExpressionAsync(getCheckboxValueScript)));
        if(checkboxValue != valueWanted)
        {
            await ClickAsync(selector);
        }
    }
    
    public async Task TypeAsync(string selector, string text) 
    {
        await WaitForSelectorThenIdlePlusTimeAsync(selector);
        var query = GetQuerySelectorScript(selector);
        var script = $"{query}.value";
            
        async Task<bool> IsTextEqual()
        {
            var text2 = ((string)(await _page.EvaluateExpressionAsync(script)));
            var result = (text.Equals(text2));

            if (result) return result;
            var isEqual = $"{script} == '{text}'";
            var output = await _page.EvaluateExpressionAsync(isEqual);
            result = (bool)output;
            return result;
        }

        await _page.TypeAsync(selector, text);
        if (await IsTextEqual()) { return; }
        
        await ClickAsync(selector);
        await _page.Keyboard.TypeAsync(text);
        if (await IsTextEqual()) { return; }

        await _page.EvaluateExpressionAsync($"{script} = '{text}'");
        if (await IsTextEqual()) { return; }
        throw new Exception($"Failed to write text to {selector}");
    }
    #endregion

    #region Collection

    private async Task GoToBottomOfScreen(int waitTimeInMs)
    {
        async Task<int> GetCurrentMaxHeight() => 
            ((int)(await _page.EvaluateExpressionAsync("document.body.scrollHeight")));
        const string scrollScript = "window.scrollTo(0, document.body.scrollHeight)";
        int current = GetCurrentMaxHeight().Result;
        int newest = current;
        do
        {
            await _page.EvaluateExpressionAsync(scrollScript);
            await _page.WaitForTimeoutAsync(waitTimeInMs);
            current = newest;
            newest = GetCurrentMaxHeight().Result;
        }
        while (current != newest);
    }

    public async Task<List<string>> GetStringListFromScript(string script)
    {
        await GoToBottomOfScreen(1000);
        var list = await _page.EvaluateExpressionAsync<List<string>>(script);
        return list;
    }
    
    public async Task<HashSet<string>> GetHashsetFromScript(string getListScript, int waitTime = 10000)
    {
        await GoToBottomOfScreen(waitTime);
        var itemsInScript = await GetStringListFromScript(getListScript);
        HashSet<String> uniqueListInformation = new HashSet<String>();
        if (uniqueListInformation == null) throw new ArgumentNullException(nameof(uniqueListInformation));
        itemsInScript.ForEach(item => uniqueListInformation.Add(item));
        return uniqueListInformation;
    }
        
    public async Task<Dictionary<string,string>> GetDictionaryFromList(string getListScript, int waitTime = 10000,  char splitter = ',')
    {
        async Task<int> GetCurrentMaxHeight() => ((int)(await _page.EvaluateExpressionAsync("document.body.scrollHeight")));
        const string scrollScript = "window.scrollTo(0, document.body.scrollHeight)";
        int current = GetCurrentMaxHeight().Result;
        int newest = current;
        var dictionary = new Dictionary<string, string>();
        do
        {
            var list2 = await GetStringListFromScript(getListScript);
            list2.ForEach(item =>
            {
                var str = item.Split(splitter);
                dictionary[str.First()] = str.Last();
            });
            await _page.EvaluateExpressionAsync(scrollScript);
            await _page.WaitForTimeoutAsync(waitTime);
            current = newest;
            newest = GetCurrentMaxHeight().Result;
        }
        while (current != newest);
        return dictionary;
    }

    public async Task<Dictionary<string, string>> GetDictionaryFromScript(string script, int waitTime = 10000)
    {
        await GoToBottomOfScreen(waitTime);
        var dictionary = await _page.EvaluateExpressionAsync<Dictionary<string, string>>(script);
        return dictionary;
    }
    #endregion
}